package com.jacie;

import org.apache.commons.compress.compressors.xz.XZCompressorInputStream;
import org.apache.commons.compress.compressors.xz.XZCompressorOutputStream;

import java.io.*;
import java.util.Objects;

public class LZMAFile {
    public static void compressFile(String sourceFile, String compressedFile) throws IOException {
        FileInputStream fis = new FileInputStream(sourceFile);
        FileOutputStream fos = new FileOutputStream(compressedFile);
        XZCompressorOutputStream xzOut = new XZCompressorOutputStream(fos);

        byte[] buffer = new byte[1024];
        int len;
        while ((len = fis.read(buffer)) != -1) {
            xzOut.write(buffer, 0, len);
        }

        fis.close();
        xzOut.close();
        fos.close();
    }

    public static void decompressFile(String compressedFile, String decompressedFile) throws IOException {
        FileInputStream fis = new FileInputStream(compressedFile);
        XZCompressorInputStream xzIn = new XZCompressorInputStream(fis);
        FileOutputStream fos = new FileOutputStream(decompressedFile);

        byte[] buffer = new byte[1024];
        int len;
        while ((len = xzIn.read(buffer)) != -1) {
            fos.write(buffer, 0, len);
        }

        xzIn.close();
        fis.close();
        fos.close();
    }

    public static void main(String[] args) {
        String sourceFile = Objects.requireNonNull(LZMAFile.class.getResource("/data.txt")).getPath();
        String compressedFile = "compressed.xz";
        String decompressedFile = "decompressed.txt";

        try {
            compressFile(sourceFile, compressedFile);
            System.out.println("File compressed successfully.");

            decompressFile(compressedFile, decompressedFile);
            System.out.println("File decompressed successfully.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
