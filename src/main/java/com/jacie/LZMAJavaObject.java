package com.jacie;

import org.apache.commons.compress.compressors.xz.XZCompressorInputStream;
import org.apache.commons.compress.compressors.xz.XZCompressorOutputStream;

import java.io.*;
import java.util.ArrayList;

/**
 * 我们使用 ObjectOutputStream 和 ObjectInputStream 来实现对象的序列化和反序列化。
 * 请确保要序列化的对象是可序列化的（即实现了 Serializable 接口）。
 * 同时，我还添加了两个辅助方法 readObjectFromFile 和 writeObjectToFile ，用于从文件中读取对象和将对象写入文件。这些方法可以在需要时进行重用。
 */
public class LZMAJavaObject {
    public static void compressObject(Object object, String compressedFile) throws IOException {
        FileOutputStream fos = new FileOutputStream(compressedFile);
        ObjectOutputStream oos = new ObjectOutputStream(new XZCompressorOutputStream(fos));
        oos.writeObject(object);
        oos.close();
        fos.close();
    }

    public static Object decompressObject(String compressedFile) throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(compressedFile);
        ObjectInputStream ois = new ObjectInputStream(new XZCompressorInputStream(fis));
        Object object = ois.readObject();
        ois.close();
        fis.close();
        return object;
    }

    public static void main(String[] args) throws IOException {
        String sourceFile = "dataObject.txt";
        Object dataObject = getResourceObject();
        writeObjectToFile(dataObject,sourceFile);
        String compressedFile = "compressedObject.xz";
        String decompressedFile = "decompressedObject.txt";

        try {
            // 压缩对象
            Object object = readObjectFromFile(sourceFile);
            compressObject(object, compressedFile);
            System.out.println("Object compressed successfully.");

            // 解压缩对象
            Object decompressedObject = decompressObject(compressedFile);
            writeObjectToFile(decompressedObject, decompressedFile);
            System.out.println("Object decompressed successfully.");
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * 模拟序列化数据
     */
    private static Object getResourceObject() {
        ArrayList<String> list = new ArrayList<>();
        int i = 100000;
        for (int i1 = 0; i1 < i; i1++) {
            list.add("Hello" + i1);
            list.add("World" + i1);
        }
        return list;
    }

    private static Object readObjectFromFile(String filePath) throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(filePath);
        ObjectInputStream ois = new ObjectInputStream(fis);
        Object object = ois.readObject();
        ois.close();
        fis.close();
        return object;
    }

    private static void writeObjectToFile(Object object, String filePath) throws IOException {
        FileOutputStream fos = new FileOutputStream(filePath);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(object);
        oos.close();
        fos.close();
    }
}
